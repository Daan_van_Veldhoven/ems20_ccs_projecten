#include <stdint.h>
#include <stddef.h>


/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/i2c/I2CCC32XX.h>

/* Example/Board Header files */
#include "Board.h"

#define Manufacturer_ID 0xFE

/*
 *  ============= mainThread ===============
 *
 *  Dit is de 'main' functie van het project
 *  Debugger is ingesteld om hier te te pauzeren
 *  i.p.v. bij de originele main-functie
 *
 *  Tips:
 *  1. Controleer de 'Linked Resources'
 *     onder 'project properties' om
 *     compilerproblemen te voorkomen
 *
 *  2. Gebruik 'Board.html' uit het project om
 *     de pinnamen te zien van de driver
 *
 */
void *mainThread(void *arg0)
{
    GPIO_init();
    I2C_init();
    I2C_Handle i2cHandle;
    I2C_Params i2cParams;
    bool status;
    i2cHandle = I2C_open(0, NULL);
    if (i2cHandle == NULL) {
        // Error opening I2C
        while (1){}
    }

    I2C_Transaction i2cTransaction = {0};

    //READ & WRITE

    uint8_t writeBuffer[1];                 //Creation writebuffer
    uint8_t readBuffer[2];
    writeBuffer[0] = Manufacturer_ID;                  //Pointer address
    i2cTransaction.slaveAddress = 0x41;     //Fills in slave_address
    i2cTransaction.writeBuf = writeBuffer;  //Fills in pointer addresses
    i2cTransaction.writeCount = 3;          //Amount of addresses
    i2cTransaction.readBuf = readBuffer;
    i2cTransaction.readCount = 5;
    status = I2C_transfer(i2cHandle, &i2cTransaction);
    if (status == false) {
        while(1);
//        Unsuccessful I2C transfer
    }
    GPIO_toggle(Board_GPIO_LED0);
    while(1);
}
