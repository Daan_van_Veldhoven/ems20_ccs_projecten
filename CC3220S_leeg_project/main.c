#include <stdint.h>
#include <stddef.h>
#include "register_def.h"
#include <gpio.h>
#include <pin.h>
#include <prcm.h>
//Extra includes hier onder


typedef enum{a,b,c,d,e,}switches;
switches leds = e;


void wait(int time_remain){ //80MHz = 1 sec
    while(time_remain != 0){

        time_remain--;

    }

}

void ledShow(leds){

    while(1){

        switch(leds){
        case a:
            HWREG(GPIOA1_BASE + (GPIO_O_GPIO_DATA) + (0b1110<<2)) ^= 0b1000; //green
            wait(2000000);

            leds = b;
            break;
        case b:
            HWREG(GPIOA1_BASE + (GPIO_O_GPIO_DATA) + (0b1110<<2)) ^= 0b0100; //yellow
            wait(1000000);
            leds = c;
            break;
        case c:
            HWREG(GPIOA1_BASE + (GPIO_O_GPIO_DATA) + (0b1110<<2)) ^= 0b0010; //red
            wait(2000000);

            leds = d;
            break;
        case d:
            wait(4000000);
            HWREG(GPIOA1_BASE + (GPIO_O_GPIO_DATA) + (0b1110<<2)) ^= 0b0010; //red
            wait(1000000);
            HWREG(GPIOA1_BASE + (GPIO_O_GPIO_DATA) + (0b1110<<2)) ^= 0b1000; //green
            wait(1000000);
            HWREG(GPIOA1_BASE + (GPIO_O_GPIO_DATA) + (0b1110<<2)) ^= 0b0100; //yellow
            wait(2000000);
            leds = a;
            break;
        case e:
            HWREG(GPIOA1_BASE + (GPIO_O_GPIO_DATA) + (0b1110<<2)) ^= 0b1000; //green
            wait(200000);
            HWREG(GPIOA1_BASE + (GPIO_O_GPIO_DATA) + (0b1110<<2)) ^= 0b0100; //yellow
            wait(150000);
            HWREG(GPIOA1_BASE + (GPIO_O_GPIO_DATA) + (0b1110<<2)) ^= 0b0010; //red
            wait(100000);
            HWREG(GPIOA1_BASE + (GPIO_O_GPIO_DATA) + (0b1110<<2)) ^= 0b0100; //yellow
            wait(200000);
            HWREG(GPIOA1_BASE + (GPIO_O_GPIO_DATA) + (0b1110<<2)) ^= 0b1000; //green
            wait(150000);
            HWREG(GPIOA1_BASE + (GPIO_O_GPIO_DATA) + (0b1110<<2)) ^= 0b0010; //red
            wait(100000);
            break;
        }

    }
}

int main(void)
{
//    ledShow(leds);
    PRCMPeripheralClkEnable(PRCM_GPIOA1 ,PRCM_RUN_MODE_CLK); //clck enable

    PinTypeGPIO(PIN_MODE_1, PIN_DIR_MODE_OUT, PIN_STRENGTH_6MA);
    PinTypeGPIO(PIN_MODE_2, PIN_DIR_MODE_OUT, PIN_STRENGTH_6MA); //current
    PinTypeGPIO(PIN_MODE_3, PIN_DIR_MODE_OUT, PIN_STRENGTH_6MA);


    GPIODirModeSet(GPIOA1_BASE ,GPIO_PIN_1, GPIO_DIR_MODE_OUT);
    GPIODirModeSet(GPIOA1_BASE ,GPIO_PIN_2, GPIO_DIR_MODE_OUT); //dir
    GPIODirModeSet(GPIOA1_BASE ,GPIO_PIN_3, GPIO_DIR_MODE_OUT);

    GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_1, GPIO_PIN_1);  //enable/disable red
    GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_2, GPIO_PIN_2);  //enable/disable yellow
    GPIOPinWrite(GPIOA1_BASE, GPIO_PIN_3, !GPIO_PIN_3); //enable/disable green



//    HWREG(GPIOA1_BASE + (APPS_RCM_O_GPIO_B_CLK_GATING)) = 1;             //Enable clock
//
//    HWREG(OCP_SHARED_BASE + (OCP_SHARED_O_GPIO_PAD_CONFIG_9)) = 0x60;   //60mA
//    HWREG(OCP_SHARED_BASE + (OCP_SHARED_O_GPIO_PAD_CONFIG_10)) = 0x60;  //60mA
//    HWREG(OCP_SHARED_BASE + (OCP_SHARED_O_GPIO_PAD_CONFIG_11)) = 0x60;  //60mA
//
//    HWREG(GPIOA1_BASE + GPIO_O_GPIO_DIR) = 0b1110;                      //led: G,Y,R,nc
//    HWREG(GPIOA1_BASE + (GPIO_O_GPIO_DATA) + (0b1110<<2)) = 0b1010;

    while(1);

    return(0);
}


