#include <stdint.h>
#include <stddef.h>


/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/i2c/I2CCC32XX.h>

/* Example/Board Header files */
#include "Board.h"

#define V_object  0x00
#define T_ambient  0x01

/*
 *  ============= mainThread ===============
 *
 *  Dit is de 'main' functie van het project
 *  Debugger is ingesteld om hier te te pauzeren
 *  i.p.v. bij de originele main-functie
 *
 *  Tips:
 *  1. Controleer de 'Linked Resources'
 *     onder 'project properties' om
 *     compilerproblemen te voorkomen
 *
 *  2. Gebruik 'Board.html' uit het project om
 *     de pinnamen te zien van de driver
 *
 */
void *mainThread(void *arg0)
{
    GPIO_init();
    I2C_init();
    int i;
    uint8_t voltageReadings[2];
    uint8_t tempReadings[2];
    int16_t voltageRaw;
    int16_t tempRaw;
    I2C_Handle i2cHandle;
    I2C_Params i2cParams;
    bool status;
    i2cHandle = I2C_open(0, NULL);
    if (i2cHandle == NULL){
        // Error opening I2C
        while (1){}
    }

    I2C_Transaction i2cTransaction = {0};

    uint8_t writeBuffer[2];                 //Creation writebuffer
    uint8_t readBuffer[2];                  //Creation readbuffer
    while(1){

        i2cTransaction.slaveAddress = 0x41;     //Fills in slave_address


        //READ & WRITE V_object

        writeBuffer[0] = V_object;              //Pointer address
        i2cTransaction.writeBuf = writeBuffer;  //Fills in pointer addresses
        i2cTransaction.writeCount = 2;          //Amount of addresses
        i2cTransaction.readBuf = readBuffer;
        i2cTransaction.readCount = 2;
        status = I2C_transfer(i2cHandle, &i2cTransaction);
        if (status == false) {
            while(1);//Unsuccessful I2C transfer
        }

        for(i = 0; i < 2; i++){
            voltageReadings[i] = readBuffer[i];
        }

        voltageRaw |= (voltageReadings[0]<<8) | (voltageReadings[1]<<0);

        //READ & WRITE T_ambient

        writeBuffer[0] = T_ambient;
        i2cTransaction.writeBuf = writeBuffer;  //Fills in pointer addresses
        i2cTransaction.writeCount = 2;          //Amount of addresses
        i2cTransaction.readBuf = readBuffer;
        i2cTransaction.readCount = 2;
        status = I2C_transfer(i2cHandle, &i2cTransaction);
        if (status == false) {
            while(1);//Unsuccessful I2C transfer
        }

        for(i = 0; i < 2;i++){
            tempReadings[i] = readBuffer[i];
        }

        tempRaw |= (tempReadings[0]<<8) | (tempReadings[1]<<0);

        GPIO_toggle(Board_GPIO_LED0);

    }



    GPIO_toggle(Board_GPIO_LED0);
    while(1);
}
