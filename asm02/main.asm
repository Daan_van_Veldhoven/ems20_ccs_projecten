;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------

            clr.b   &DCOCTL; Select lowest DCOx and MODx settings
            mov.b   &CALBC1_1MHZ,&BCSCTL1 ; Set range
            mov.b   &CALDCO_1MHZ,&DCOCTL ; Set DCO step + modulation

			clr		&P1OUT			;
            bis.b   #0x03,&P1DIR 	;Set bit 1 and 2 in register P1DIR

loop2		xor.b	#0x02,&P1OUT	;
			mov.w	#9,r14			;
			jmp		telaf1			;

loop1       xor.b   #0x01,&P1OUT 	;Toggle bit 1 in register P1OUT
            mov.w   #50000,r15 		;Move value 50000 into global register 15 (1 clock-cycle)

telaf2		dec.w	r14				;
			jz		loop2

telaf1      dec.w   r15 			;Decrease value of global register 15 by 1 (1 clock-cycle)
            jnz     telaf1 			;Jump to telaf1 if Z = 0 (2 clock-cycle)
            jmp		loop1			;



                                            
;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
