        .def    mul
        .def	pow
        .def	pow2
        .text
mul     clr.w   r14
next    clrc
        rrc.w   r12
        jnc     no_add
        add.w   r13,r14; saves r13, end value
no_add  add.w   r13,r13
        tst.w   r12
        jne     next
        mov.w   r14,r9 ; POW
        mov.w	r14,r12 ; MUL
        ret

;14 = savepoint for r13 and will return answer r12 --- MUL
;13 = n and adder --- MUL
;12 = answer and x --- MUL
;11 = Result Previous Mul (RVP) --- POW
;10 = savepoint for x --- POW
;9 = savepoint for answer --- POW
;8 = --- POW2
;7 = --- POW2
;6 = --- POW2

pow		clr.w	r11
		clr.w	r10
		clr.w	r9
		mov.w	r12,r11
		mov.w	r13,r10

newit	dec.w	r10
		jnz		calc
		jmp		end

calc	mov.w	r11,r13
		call	#mul
		mov.w	r9,r12
		jmp		newit

end		mov.w	r9,r12
		ret

pow2	clr.w	r8 ;n
		clr.w	r7 ;x
		clr.w	r6 ;res
		mov.w	r12,r7
		mov.w	r13,r8
		bis		#1,r6

newit2	tst.w	r8
		jz		end2
		clrc
		rrc.w	r8
		jnc		calc2
		mov.w	r7,r12
		mov.w	r6,r13
		call	#mul
        mov.w	r14,r6

calc2	mov.w	r7,r12
		mov.w	r7,r13
		call	#mul
		mov.w	r14,r7

end2	mov.w	r6,r12
		ret

