#include <msp430.h>

extern int mul(int x, int n);
extern int pow(int x, int n);
extern int pow2(int x, int n);

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;

    volatile int a = 0;
    volatile int b = 0;
    volatile int c = 0;

    a = mul(2,3);
    b = pow(3,9);
    c = pow2(3,1);


    while (1);
}
