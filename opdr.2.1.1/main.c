#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#define AANTAL_SPELERS_IN_LIJST_LAATSTE 3
#define AANTAL_SPELERS_IN_LIJST_BEST 5
#define MAX_AANTAL_KARAKTERS_IN_NAAM 15

typedef struct {
    char naam[MAX_AANTAL_KARAKTERS_IN_NAAM + 1]; //+1 vanwege \0?
    unsigned int punten;
} Speler;

void maakLijstLeeg(Speler lijst[], size_t aantalInLijst)
{
    for (size_t i = 0; i < aantalInLijst; i++)
    {
        size_t ci;
        for (ci = 0; ci < MAX_AANTAL_KARAKTERS_IN_NAAM; ci++)
        {
            lijst[i].naam[ci] = '-';
        }
        lijst[i].naam[ci] = '\0';
        lijst[i].punten = 0;
    }
}

void printLijst(Speler lijst[], size_t aantalInLijst)
{
    for (size_t i = 0; i < aantalInLijst; i++)
    {
        printf("%-*s %5d\n", MAX_AANTAL_KARAKTERS_IN_NAAM, lijst[i].naam, lijst[i].punten);
    }
}

void zetBovenaanInLijst(Speler s, Speler lijst[], size_t aantalInLijst, int lim)
{
    // Alle voorgaande spelers een plaatsje opschuiven:
    for (size_t i = aantalInLijst - 1; i > lim; i--)
    {
        lijst[i] = lijst[i - 1];
    }
    // De laatste speler komt bovenaan de lijst:
    lijst[lim] = s;
}

int speelSpel(void)
{
    // Hier komt  de code die het spel bestuurt.
    // Het aantal behaalde punten wordt als returnwaarde teruggegeven.
    return rand();
}

void leesNaam(char s[])
{
    char karakter;
    size_t i = 0;
    do
    {
        karakter = getchar();
        if (isprint(karakter) && i != MAX_AANTAL_KARAKTERS_IN_NAAM)
        {
            s[i] = karakter;
            i++;
        }
    }
    while (karakter != '\n');
    s[i] = '\0';
}

void highscore(Speler newPoint, Speler topscores[], int aantaltopscores){
    int i;
    for(i = 0 ; i < aantaltopscores ; i++){
        if(topscores[i].punten == NULL || topscores[i].punten < newPoint.punten){
            zetBovenaanInLijst(newPoint, topscores, aantaltopscores, i);
            break;
        }
    }
}

int main(void)
{
    srand(time(NULL));
    Speler laatsteSpelers[AANTAL_SPELERS_IN_LIJST_LAATSTE];
    Speler bestSpelers[AANTAL_SPELERS_IN_LIJST_BEST];
    maakLijstLeeg(laatsteSpelers, AANTAL_SPELERS_IN_LIJST_LAATSTE);
    maakLijstLeeg(bestSpelers, AANTAL_SPELERS_IN_LIJST_BEST);

    while (1)
    {
        Speler s;
        s.punten = speelSpel();
        printf("Je hebt %d punten behaald, type nu je naam in: ", s.punten);
        leesNaam(s.naam);
        zetBovenaanInLijst(s, laatsteSpelers, AANTAL_SPELERS_IN_LIJST_LAATSTE, 0);
        highscore(s, bestSpelers, AANTAL_SPELERS_IN_LIJST_BEST);

        printf("Laatste %d spelers:\n", AANTAL_SPELERS_IN_LIJST_LAATSTE);
        printLijst(laatsteSpelers, AANTAL_SPELERS_IN_LIJST_LAATSTE);
        printf("Beste %d spelers:\n", AANTAL_SPELERS_IN_LIJST_BEST);
        printLijst(bestSpelers, AANTAL_SPELERS_IN_LIJST_BEST);
    }
    return 0;
}
