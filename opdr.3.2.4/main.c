#include <stdint.h>
#include <stddef.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/UART.h>
#include <ti/drivers/uart/UARTCC32XX.h>

/* Example/Board Header files */
#include "Board.h"

/*
 *  ============= mainThread ===============
 *
 *  Dit is de 'main' functie van het project
 *  Debugger is ingesteld om hier te te pauzeren
 *  i.p.v. bij de originele main-functie
 *
 *  Tips:
 *  1. Controleer de 'Linked Resources'
 *     onder 'project properties' om
 *     compilerproblemen te voorkomen
 *
 *  2. Gebruik 'Board.html' uit het project om
 *     de pinnamen te zien van de driver
 *
 */
void *mainThread(void *arg0)
{
    char        input;
    UART_Handle uart;
    UART_Params uartParams;
    // Initialize the UART driver.  UART_init() must be called before
    // calling any other UART APIs.
    UART_init();
    // Create a UART with data processing off.
    UART_Params_init(&uartParams);
    uartParams.writeDataMode = UART_DATA_BINARY;
    uartParams.readDataMode = UART_DATA_BINARY;
    uartParams.readReturnMode = UART_RETURN_FULL;
    uartParams.readEcho = UART_ECHO_ON;
    uartParams.baudRate = 2400;
    // Open an instance of the UART drivers
    uart = UART_open(Board_UART0, &uartParams);
    if (uart == NULL) {
        // UART_open() failed
        while (1);
    }
    GPIO_init();
    // Loop forever echoing
    while (1) {
        UART_read(uart, &input, 1);
        GPIO_toggle(Board_GPIO_LED0);
    }



    while(1)
    {
        GPIO_toggle(Board_GPIO_LED0);
    }
}
