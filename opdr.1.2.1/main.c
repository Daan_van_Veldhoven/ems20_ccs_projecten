#include <stdio.h>
#include <stdbool.h>
#include <math.h>

bool isPrime(int n)
{
    if (n < 2)
    {
        return false;
    }
    int i;
    for (i = 2; i <= sqrt(n); i++)
    {
        if (n % i == 0)
        {
            return false;
        }
    }
    return true;
}

int omliggendePrime(int n, int *hoger, int *lager){
    int h = n+1;
    int l = n-1;
    while(!isPrime(h)){
        h++;
    }
    while(!isPrime(l)){
        l--;
    }

    *hoger = h;
    *lager = l;

}

int main(void)
{
    int number;
    int h_ans, l_ans;
    printf("Geef een geheel number: ");
    scanf("%d", &number);
    printf("Het number %d is ", number);
    if (isPrime(number))
    {
        printf("een ");
    }
    else
    {
        printf("geen ");
    }
    printf("priemgetal.\n");
    omliggendePrime(number, &h_ans, &l_ans);
    printf("De omliggende priemgetallen zijn: %d en %d\n", l_ans, h_ans);
    while (1);
    return 0;
}
