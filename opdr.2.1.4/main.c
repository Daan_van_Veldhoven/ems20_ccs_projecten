#include <stdio.h>

typedef struct {
    int type;
    int index;
    double waarde;
} PassieveComponent;

typedef enum{Resistor, Capacitor, Coil}type;

typedef enum{}

void printComponent(PassieveComponent c)
{
    type component;
    switch (c.type)
    {
    case 1:
        printf("R%d = %.1E ohm", c.index, c.waarde);
        break;
    case 2:
        printf("C%d = %.1E farad", c.index, c.waarde);
        break;
    case 3:
        printf("L%d = %.1E henry", c.index, c.waarde);
        break;
    }
}

int main(void)
{
    PassieveComponent c[] = {
        {Resistor, 1, 1E6}, {Resistor, 2, 2.7E3}, {Capacitor, 1, 1E-9}, {Coil, 1, 1E-3}, {Coil, 2, 15E-6}
    };
    size_t i;
    printf("\n");
    for (i = 0; i < sizeof c / sizeof c[0]; i++)
    {
        printComponent(c[i]);
        printf("\n");
    }

    while (1);
    return 0;
}
